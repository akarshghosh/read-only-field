# Read-Only-Field

DatoCMS Plugin for making the fields only readable

## Tools Required

[NodeJS](https://nodejs.org/en/download/) For deploying a server

[ngrok](https://ngrok.com/download) For creating a public HTTPS URL for file running locally on the development machine.

## Steps

1)	Deploy a server (typically a node server using express.js)

```
const express = require('express')
const app = express()
app.listen(3000, () => {
    console.log('Server started at port 3000...')
})
```
2)	Create a HTML file with the following template

```
<!doctype html>
<html lang="en">
<head>
  <meta charset="UTF-8" />
  <script src="https://unpkg.com/datocms-plugins-sdk"></script>
  <link href="https://unpkg.com/datocms-plugins-sdk/dist/sdk.css" media="all" rel="stylesheet" />
</head>
<body>
  <input type="text" />
  <script type="text/javascript">
    	//////////////////////////////////
		CODE FOR PLUGIN HERE
	//////////////////////////////////
  </script>
</body>
</html>
```

4)	Refer links to write plugins

Entry points:[Click Here](https://www.datocms.com/docs/building-plugins/entry-point)

Writing Plugins:[Click Here] (https://www.datocms.com/docs/building-plugins/sdk-reference)

```
<script type="text/javascript">
    DatoCmsPlugin.init(function(plugin) {
      const input = document.querySelector("input");
      plugin.startAutoResizer()
      input.value = plugin.getFieldValue(plugin.fieldPath);

      document.querySelector("input").disabled=true
      console.log(plugin.itemId);
    });
  </script>
```

5)	Provide a route to retrieve the above HTML file
```
const express = require('express')
const app = express()

app.get('/', (req,res)=>{
    res.sendFile('index.html', {root: __dirname})
})

app.listen(3000, () => {
    console.log('Server started at port 3000...')
})

```

6)	Start the Server.

[NodeMon](https://www.npmjs.com/package/nodemon) can be used for monitoring changes in the file.

7) Open ngrok application and type the following command.

```
 ngrok http 3000
```
You'll see the below window.
```
Session Status                online            
Session Expires               7 hours, 59 minutes                                                                       
Version                       2.3.35                                                                                    
Region                        United States (us)                                                                        
Web Interface                 http://127.0.0.1:4040                                                                     
Forwarding                    http://13f9d4fa22c4.ngrok.io -> http://localhost:3000                                     
Forwarding                    https://13f9d4fa22c4.ngrok.io -> http://localhost:3000          

```
COpy the above https link and use that as an Entry-Point URL in DatoCMS Plugin